// import { writeFileSync } from 'fs'
const form =document.querySelector('form')
const msg=document.querySelector('.msg')

// const hide=document.querySelector('.msga')
// console.log(hide);
// hide.addEventListener('click',(e)=>{
//    e.preventDefault()
//    msg.classList.add('hidden')
// })
form.addEventListener('submit',(e)=>{
    e.preventDefault()
    
    let idata=e.target.querySelectorAll('input')
    let btn=e.target.querySelectorAll('button')
    let formdata={}
    for(let i=0;i<idata.length;i++){
        let value=idata[i].value;
        let name=idata[i].name;
        if(value){
            formdata[name]=value
        }else{
            formdata[name]=null
        }
    }
    let checkPass=checkPassword(formdata.password) 
    if(checkPass!==true){
        console.error(checkPass)
        msg.textContent=checkPass
        msg.classList.remove('hidden')
    }
    const btnvalue=e.submitter.name
    if(btnvalue==='login' && checkPass===true){
        checkEmail(formdata.email)
    }
    // if(btnvalue==='signup'){
    //  if(checkPass===true)
    //   writedata(formdata)
    // else{
    //     console.error('Invalid password')
    //     msg.textContent='Invalid Password'
    //     msg.classList.remove('hidden')
    // }
    // }
})

// for signup
const writedata=(formdata)=>{
    console.log(formdata,'inside writedata');
    fetch("./database.json").
    then((res)=>res.json()).
    then((data)=>{
        let {name,email,password}=formdata
        data[email]={
            name,
            password
        }
        console.log(data);
        // writeFileSync("./database.json",JSON.stringify(data))
        
    })
    .catch((err)=>console.log(err))

}

// to check email from database.json
const checkEmail=(email)=>{
    fetch('./database.json').
    then((res)=>res.json()).
    then((data)=>{
        if(data[email]){
            console.log( 'valid Email');
            return true;
        }else{
            console.log( 'Invalid Email');
            msg.textContent='Invalid Email'
            msg.classList.remove('hidden')
            return false;
        }
    }).then((res)=>{
        if(res)
        window.location.href="./login.html"
        else{
            throw new Error("Invalid Email");
        }
    })
    
    .catch((err)=>console.log(err))
}
// to check password
const checkPassword=(str)=>{
    let length=str.length;
    if(length<10 || length>15)
    return 'either password is too short or too long'
    //check number
    let num=false;
    let upp=false;
    let low=false;
    for(let i=0;i<length;i++){
        let v=str.charCodeAt(i)
        if(v>=97 && v<=122)
        low=true;
        if(v>=48 && v<=57)
        num=true;
        if(v>=65 && v<=90)
        upp=true;   
    }
    if(num==false)
    return 'password must contain at least one number'
    if(low==false)
    return 'password must contain small-letter'
    if(upp==false)
    return 'password must contain capital-letter'
    return true;
}